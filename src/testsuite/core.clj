(ns testsuite.core
  (:require [clj-webdriver.taxi :refer :all])
  (:require [testsuite.motor.quoteform :refer :all]))

(def ^:private browser-count (atom 0))

(defn browser-up
  "Start up a browser if it's not already started."
  []
  (when (= 1 (swap! browser-count inc))
    (set-driver! {:browser :chrome})
    (implicit-wait 60000)))

(defn browser-down
  "If this is the last request, shut the browser down."
  [& {:keys [force] :or {force false}}]
  (when (zero? (swap! browser-count (if force (constantly 0) dec)))
    (quit)))


(defn go-to-motor-quote
    []
    (browser-up)
    (to "http://www.123.ie/car-insurance-quote/"))


(defn basic-data-entry
  []
  (policyStartDate "2" "Mar 2015")
  (yourDrivingExperience :PolicyInYourOwnName)
  (yearsNoClaimsBouns :years6Plus)
  (claimsInTheLast4YearsIncludingWindscreenClaims :no)
  (yourCarRegistration "")
  (carSecurity :None)
  (carKmPerYear :kmLow)
  (carValue "€ 1,000"))