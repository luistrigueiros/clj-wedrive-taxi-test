(ns testsuite.motor.quoteform
  (:require [testsuite.motor.model :refer :all])
  (:require [clj-webdriver.taxi :refer :all])
  (:require [clojure.pprint :refer :all]))
                                       

(defn vscroll 
  [y]
  (execute-script (format "window.scrollBy(0,%d)" y)))

(defn scrollToElement
  [selector]
    (let [xy (location (element selector))] 
      (vscroll (:y xy))))

(defn drop-box-select 
  [optionsMap cssSelector value]
  (let [optionToSelect (optionsMap value)]
    (if (nil? optionToSelect) 
      (pprint optionsMap)
      (select-by-text cssSelector optionToSelect))))



(defn policyStartDate
    [start-day monthYear]
    (scrollToElement "#policyStartDay")
    (select-option "#policyStartDay" {:value start-day})
    (select-by-text "#policyStartMonthYear" monthYear))

(defn yourDrivingExperience
  ([] 
    (pprint  YOUR_DRIVING_EXPERINCE))
  ([drivingType] 
    (drop-box-select YOUR_DRIVING_EXPERINCE "#drivingExperienceType" drivingType)))

(defn yearsNoClaimsBouns
  ([]
    (pprint YEARS_NO_CLAIMSBOUNS))
  ([years]
    (drop-box-select YEARS_NO_CLAIMSBOUNS "#experienceYears" years)))

(defn claimsInTheLast4YearsIncludingWindscreenClaims 
  [yesNo]
  (if (= yesNo :yes)
    (click "#hasClaimsY")
    (click "#hasClaimsN")))

(defn yourLicenseType
  ([]
    (pprint YOUR_LICENSE_TYPE))  
  ([value]
    (drop-box-select YOUR_LICENSE_TYPE "#licenseType" value)))

(defn yearsHeldLicense
  [value]
  (let [selection (value YEARS_HELD_LICENSE)]
    (select-by-text "#yearsHeld" selection)))

(defn yourCarRegistration
  [registration]
  (input-text "#registration" "99D68143")
  (click "#carLookupButton"))

(defn carSecurity
  ([]
    (pprint CAR_SECURITY))
  ([value]
  (drop-box-select CAR_SECURITY "#security" value)))

(defn carKmPerYear
  ([]
    (pprint CAR_KM_PER_YEAR))
  ([value]
  (drop-box-select CAR_KM_PER_YEAR "#mileage" value)))

(defn carValue
  [value]
  (select-by-text "#valueOfCar" value))

(defn carClassofUse
  [classOfUse]
  (if (= classOfUse :socialDomesticPleasure)
    (click "#classOfUseB")
    (click "#classOfUseC")))

(defn yearsDrivingIrelandUK
  [value]
  (let [selection (value YEARS_DRIVING_IRELANDUK)]
    (select-by-text "#livingIrelandUK" selection)))

(defn yourTitle
  [value]
  (let [selection (value YOUR_TITLE)]
    (select-by-text "#title" selection)))

(defn dateOfBirth
    [day month year]
    (select-by-text "#birthDay" day)
    (select-by-text "#birthMonth" month)
    (select-by-text "#birthYear" year))



(defn fill-in-address
  []
  (input-text "#address1" "aa")
  (input-text "#address2" "aa")
  (input-text "#address3" "aa")
  (select-by-text "#county" "Cavan")
  (click "#btnMatchGeocoding")
  (Thread/sleep 4000)
  (wait-until #(exists? "#geocodeMatchedAddressList > div > div.inputArea.chooseAddress"))
  ; The address found by geoding. maybe someother why of selecting would be better
  (click "#choose0"))


(defn simple-quote
    []
    
    ;(select-by-index "#livingIrelandUK" 1)
    (select-by-text "#livingIrelandUK" "Less than 1 year")
    (click "#hasPenaltyPointsN")
    (input-text "#firstname" "Luis")
    (input-text "#surname" "Trigueiros")
    (input-text "#email" "oscar.trigueiro@123.ie")
    (input-text "#phoneNumber" "087123456")
    (select-by-text "#employmentStatus" "Employed/Self Employed")
    (click "#hasHouseNameN")
    (click "#homeOwnerN")
    (click "#hasAdditionalDriversN")
    ;(click "#btnQuote")
    )

