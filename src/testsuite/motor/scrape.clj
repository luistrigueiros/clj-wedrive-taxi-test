(ns testsuite.motor.scrape
  (:require [net.cgrand.enlive-html :as html]))

(def ^:dynamic *base-url* "https://www.123.ie/insurance/car-insurance/quote.123#/quote-form")

(defn fetch-url [url]
  (html/html-resource (java.net.URL. url)))

(defn hn-headlines []
  (map html/text (html/select (fetch-url *base-url*) [:select :option])))


